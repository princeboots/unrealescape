// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorOpener.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Components/ActorComponent.h"
#include "Components/PrimitiveComponent.h"

#define OUT

// Sets default values for this component's properties
UDoorOpener::UDoorOpener()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UDoorOpener::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	InitialRotation = FRotator(Owner->GetActorTransform().GetRotation());
	OpenRotation += InitialRotation;

	ActorThatOpensDoor = GetWorld()->GetFirstPlayerController()->GetPawn();
}


// Called every frame
void UDoorOpener::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	float Time = GetWorld()->GetTimeSeconds();

	if (PressurePlate)
	{
		for (auto* Plate : PressurePlate)
		{
			if (TotalMassOnPlate(Plate) > TriggerMass)
			{
				DoorOpenStartTime = Time;
				OpenDoor();
			}
		}
	}
	/*
	if (PressurePlate[0]->IsOverlappingActor(ActorThatOpensDoor)
		|| PressurePlate[1]->IsOverlappingActor(ActorThatOpensDoor))
	{
		DoorOpenStartTime = Time;
		OpenDoor();
	}*/

	if (Time - DoorOpenStartTime > DoorCloseDelay)
	{
		CloseDoor();
	}
}

void UDoorOpener::OpenDoor() const
{
	//Owner->SetActorRotation(OpenRotation);
	OnOpenRequest.Broadcast();
}

void UDoorOpener::CloseDoor() const
{
	//Owner->SetActorRotation(InitialRotation);
	OnCloseRequest.Broadcast();
}

float UDoorOpener::TotalMassOnPlate(ATriggerVolume* Plate)
{
	float Mass = 0.0f;

	if (Plate)
	{
		TArray<AActor*> Actors;
		Plate->GetOverlappingActors(OUT Actors);

		for (const auto* Actr : Actors)
		{
			UPrimitiveComponent* Collision = Actr->FindComponentByClass<UPrimitiveComponent>();
			Mass +=	Collision->GetMass();
		}
	}
	return Mass;
}

