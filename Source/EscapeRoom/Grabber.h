// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/InputComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEROOM_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	UPROPERTY(EditAnywhere)
	float Reach = 100.0f;

	UPhysicsHandleComponent* PhysicsHandle = nullptr;
	UInputComponent* Input = nullptr;
	APlayerController* PlayerController = nullptr;
	AActor* ActorHit;

	FKey keyBinding;

	void Grab();

	void Release();

	FHitResult GetFirstPhysicsBodyInRange(float Range);
	
	FVector GetPointAtEndOfRange(float Range, FVector& ViewpointLocation);

	void LogInputActionKeyPress(EInputEvent InputEvent);

	void DrawDebugArrow(FVector &ViewpointLocation, FVector &LineTraceEnd);		
};
