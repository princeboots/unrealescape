// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "ObjectMacros.h"
#include "DoorOpener.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorEvent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPEROOM_API UDoorOpener : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDoorOpener();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenDoor() const;
	void CloseDoor() const;
	float TotalMassOnPlate(ATriggerVolume* Plate);

	UPROPERTY(BlueprintAssignable)
		FDoorEvent OnOpenRequest;

	UPROPERTY(BlueprintAssignable)
		FDoorEvent OnCloseRequest;


private:
	UPROPERTY(EditAnywhere, Category = DoorProperties)
	ATriggerVolume* PressurePlate[2];

	UPROPERTY(EditAnywhere, Category = DoorProperties)
	AActor* ActorThatOpensDoor = nullptr;

	UPROPERTY(EditAnywhere, Category = DoorProperties)
	float TriggerMass = 0.0f;

	UPROPERTY(VisibleAnywhere, Category = DoorProperties)
	FRotator OpenRotation = FRotator(0.0f, 105.0f, 0.0f);
	FRotator InitialRotation;

	UPROPERTY(EditAnywhere, Category = DoorProperties)
	float DoorCloseDelay = 1.0f;
	float DoorOpenStartTime;

	AActor* Owner;
};
