// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerInput.h"
#include "GameFramework/PlayerController.h"
#include "Components/PrimitiveComponent.h"
#include "DrawDebugHelpers.h"
#include <string>
#include <assert.h> 

#define OUT

UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	//UE_LOG(LogTemp, Warning, TEXT(" Default pawn is %s at %s"), *GetOwner()->GetName(), *GetOwner()->GetActorTransform().GetLocation().ToString());

	PlayerController = GetWorld()->GetFirstPlayerController();
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	Input = GetOwner()->FindComponentByClass<UInputComponent>();

	assert(PlayerController != nullptr);
	assert(PhysicsHandle != nullptr);
	assert(Input != nullptr);

	Input->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
	Input->BindAction("Grab", IE_Released, this, &UGrabber::Release);
}

void UGrabber::Grab()
{
	FHitResult HitResult = GetFirstPhysicsBodyInRange(Reach);

	ActorHit = HitResult.GetActor();
	if (ActorHit && PhysicsHandle)
	{
		UPrimitiveComponent* ComponentToGrab = HitResult.GetComponent();
		PhysicsHandle->GrabComponent(ComponentToGrab, NAME_None, ComponentToGrab->GetOwner()->GetActorLocation(), true);
	}

	LogInputActionKeyPress(IE_Pressed);
}

void UGrabber::Release()
{
	ActorHit = nullptr;
	if (PhysicsHandle)
	{
		PhysicsHandle->ReleaseComponent();
	}

	LogInputActionKeyPress(IE_Released);
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (PhysicsHandle)
	{
		if (PhysicsHandle->GrabbedComponent)
		{
			FVector ViewpointLocation;
			FVector TargetLocation = GetPointAtEndOfRange(Reach, ViewpointLocation);

			PhysicsHandle->SetTargetLocation(TargetLocation);
		}
	}

	//DrawDebugArrow(ViewpointLocation, LineTraceEnd);
}

FHitResult UGrabber::GetFirstPhysicsBodyInRange(float Range)
{
	FVector ViewpointLocation;
	FVector LineTraceEnd = GetPointAtEndOfRange(Range, OUT ViewpointLocation);

	FHitResult hit;
	FCollisionObjectQueryParams ObjectParams(ECollisionChannel::ECC_PhysicsBody);
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());
	GetWorld()->LineTraceSingleByObjectType(
		OUT hit,
		ViewpointLocation,
		LineTraceEnd,
		ObjectParams,
		TraceParams
	);
	return hit;
}

FVector UGrabber::GetPointAtEndOfRange(float Range, FVector& ViewpointLocation)
{
	FRotator ViewpointRotation;
	if (PlayerController)
	{
		PlayerController->GetPlayerViewPoint(
			OUT ViewpointLocation,
			OUT ViewpointRotation
		);
	}

	return ViewpointLocation + Range * ViewpointRotation.Vector();
}

void UGrabber::LogInputActionKeyPress(EInputEvent InputEvent)
{
	if (InputEvent == IE_Pressed)
	{
		TArray<FInputActionKeyMapping> Bindings = PlayerController->PlayerInput->GetKeysForAction("Grab");
		int32 max = Bindings.Max();
		for (int32 i = 0; i < max; i++)
		{
			if (PlayerController->IsInputKeyDown(Bindings[i].Key))
			{
				keyBinding = Bindings[i].Key;
			}
		}
		UE_LOG(LogTemp, Warning, TEXT(" GRAB (%s) PRESSED "), *keyBinding.ToString());
	}
	else if (InputEvent == IE_Released)
	{
		UE_LOG(LogTemp, Warning, TEXT(" GRAB (%s) RELEASED "), *keyBinding.ToString());
	}
}

void UGrabber::DrawDebugArrow(FVector &ViewpointLocation, FVector &LineTraceEnd)
{
	FColor LineColour = FColor(100, 150, 200);
	DrawDebugLine(GetWorld(), ViewpointLocation, LineTraceEnd, LineColour, false, 0.0f, 0, 10.0f);
}